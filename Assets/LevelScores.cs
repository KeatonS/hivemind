﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelScores : MonoBehaviour {

    public List<GameObject> world1Levels;
    public List<GameObject> world2Levels;
    public List<GameObject> world3Levels;
    List<List<GameObject>> allWorldLists = new List<List<GameObject>>();
    // Use this for initialization
    void Start () {
        allWorldLists.Add(world1Levels);
        allWorldLists.Add(world2Levels);
        allWorldLists.Add(world3Levels);
        PlayerPrefs.SetInt("-1/2SCORE", 1);

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                string prefsKey;
                string curWorld = "[" + i + "][" + j + "]";
                if (j == 0)
                {
                    prefsKey = (i-1) + "/" + (2) + "SCORE";

                }else
                {
                    prefsKey = (i) + "/" + (j-1) + "SCORE";
                }

                if (PlayerPrefs.GetInt(prefsKey) > 0)
                {
                    Debug.Log(curWorld + " == " + prefsKey + "| " + PlayerPrefs.GetInt(prefsKey));
                    allWorldLists[i][j].gameObject.GetComponent<Button>().interactable = true;
                }
                else
                {
                    Debug.Log(curWorld + " == " + prefsKey + "   | " + PlayerPrefs.GetInt(prefsKey));
                    allWorldLists[i][j].gameObject.GetComponent<Button>().interactable = false;
                }
                //Debug.Log(prefsKey + "| " + PlayerPrefs.GetInt(prefsKey));

            }
        }
	}
}
