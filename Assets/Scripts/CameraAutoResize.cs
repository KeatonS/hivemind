﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraAutoResize : MonoBehaviour {

    private int rows;
    private int columns;
    private bool resizeComplete = false;
	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
        if(MapGenerator.instance.doneGenerating && !resizeComplete)
        {
            columns = MapGenerator.instance.columns;
            rows = MapGenerator.instance.rows;
            float size = columns + 2;
            this.transform.position = new Vector3((columns / 2.0f) -0.5f, -((rows / 2) + (size - rows/2) ), -20);
            
            this.GetComponent<Camera>().orthographicSize = (size);
            resizeComplete = true;
        }
     
    }
}
