﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStarPath : MonoBehaviour
{

    public GameObject target;
    public GameObject owner;
    int columns = 0;
    int rows = 0;
    string mapAsString = "";
    bool empty = true;
    List<List<char>> mapChars;
    List<Node> openList; //container for considered path
    List<Node> closedList; //container for squares that dont need to be considered again
    private List<Vector2> pathList;
    char openSpaceChar = '.';
    char closedSpaceChar = '#';
    //char targetChar = 'e';

    private IEnumerator coroutine;

    int iterCt = 0;
    float totalTime = 0f;
    public class Node
    {
        public Node parent;
        public Vector2 position;

        public int g = 0, h = 0, f = 0;

        public Node(Node par, Vector2 pos)
        {
            this.parent = par;
            this.position = pos;

        }
    }


    // Use this for initialization
    void Start()
    {
        mapChars = new List<List<char>>();
        pathList = new List<Vector2>();
        // target = GameObject.FindGameObjectWithTag("Egg");
        target = null;
        owner = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (MapGenerator.instance.doneGenerating && empty && target != null && owner != null)
        {
            CopyMap();
            StartCoroutine(AStarAlgo(this.transform.position, target.transform.position));
        }
    }

    IEnumerator AStarAlgo(Vector2 start, Vector2 goal)
    {
        totalTime = Time.time;

        Node startNode = new Node(null, start);
        Vector2 posUnitsGoal = new Vector2(goal.x, Mathf.Abs(goal.y));
        Node endNode = new Node(null, posUnitsGoal);

        openList = new List<Node>();
        closedList = new List<Node>();
        pathList = new List<Vector2>();

        startNode.g = 0;
        startNode.h = 0;
        startNode.f = 0;

        openList.Add(startNode);

        while (openList.Count > 0)
        {
            iterCt++;

            Node curNode = openList[0];
            int curIndex = 0;

            // Current is openlist element with lowest fscore
            for (int i = 0; i < openList.Count; i++)
            {
                iterCt++;

                if (openList[i].f < curNode.f)
                {
                    curNode = openList[i];
                    curIndex = i;
                }
            }
            openList.RemoveAt(curIndex);
            closedList.Add(curNode);

            // Goal has been found
            if (curNode.position == endNode.position)
            {
                Node current = curNode;

                while (current != null)
                {
                    pathList.Add(current.position);
                    current = current.parent;
                    totalTime = Time.time - totalTime;

                    Debug.Log("Total time took: " + totalTime);
                    Debug.Log("Iterations: " + iterCt);
                }
                break;
            }

            List<Node> children = new List<Node>();
            List<Vector2> directions = new List<Vector2>() { Vector2.up, Vector2.right, Vector2.down, Vector2.left, };

            foreach (Vector2 newPos in directions)
            {
                iterCt++;

                Vector2 nodePos = curNode.position + newPos;

                if ((nodePos.x > columns) || (nodePos.x < 0) || (nodePos.y > rows) || (nodePos.y < 0))
                {
                    continue;
                }

                if (mapChars[(int)nodePos.x][(int)nodePos.y] == closedSpaceChar)
                {
                    continue;
                }
                Node newNode = new Node(null, nodePos);

                children.Add(newNode);
            }

            foreach (Node child in children)
            {
                iterCt++;
                foreach (Node closedChild in closedList)
                {
                    if (child.position == closedChild.position)
                    {
                        continue;
                    }
                }

                child.g = curNode.g + 1;
                child.h = (int)((Mathf.Abs((endNode.position.x - child.position.x))) + (Mathf.Abs((endNode.position.y - child.position.y))));
                child.f = child.g + child.h;


                foreach (Node openNode in openList)
                {
                    iterCt++;
                    if (child.position == openNode.position && child.g >= openNode.g)
                    {
                        continue;
                    }
                }
                child.parent = curNode;
                openList.Add(child);
            }
            yield return null;
        }
        
    }

    void CopyMap()
    {
        MapGenerator.instance.GetMapData(out columns, out rows, out mapAsString);
        ConvertMapToMatrix();
        empty = false;
        target = GameObject.FindGameObjectWithTag("Egg");
    }

    void ConvertMapToMatrix()
    {
        for (int c = 0; c < columns; c++)
        {
            List<char> sublist = new List<char>();

            for (int r = 0; r < rows; r++)
            {
                int fakeCtr = c + (r * (columns));
                char toPush;
                if (mapAsString[fakeCtr] == 'e' || mapAsString[fakeCtr] == 'r' || mapAsString[fakeCtr] == 'l' || mapAsString[fakeCtr] == 'b')
                    toPush = openSpaceChar;
                else
                    toPush = mapAsString[fakeCtr];
                sublist.Add(toPush);
            }
            mapChars.Add(sublist);
        }
    }

    void DisplayMapMatrix(List<List<char>> matrix)
    {
        for (int y = 0; y < rows; y++)
        {
            string line = "";
            for (int x = 0; x < columns; x++)
            {
                line += mapChars[x][y];
            }
            Debug.Log(line);
        }
    }

    void DisplayPath(List<Vector2> path)
    {
        foreach (Vector2 space in pathList)
        {
            Debug.Log(space);
        }
    }

    void DisplayNodeList(List<Node> nodes)
    {
        foreach (Node node in nodes)
        {
            Debug.Log(node.position);
        }
    }

    public List<Vector2> GetPathList()
    {
        return this.pathList;
    }
}
