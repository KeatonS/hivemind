﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;

using Random = UnityEngine.Random;

public class MapGenerator : MonoBehaviour {

    [HideInInspector] public static MapGenerator instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
    [HideInInspector] public int columns;
    [HideInInspector] public int rows;
    public GameObject [] levelTileSet;
    private int startingLevel = 1;
    public GameObject redAnt;
    public GameObject blueAnt;
    public PathFind.Grid grid;
    public bool doneGenerating = false;

    private string levelInASCII;
    private MapHolder mapHolder;
    private char floorChar = '.';
    private char wallChar = '#';
    private char exitChar = 'x';
    private char leftAntChar = 'l';
    private char rightAntChar = 'r';
    private char eggChar = 'e';

    private Transform boardHolder;
    private LevelSet myLevelSet;

    private Vector2 leftAntPos;
    private Vector2 rightAntPos;
    private bool[,] tilesMapBool;
    private float[,] tilesMapFloat;

    private enum Mode {
        boolMode,
        floatMode
    }
    Mode curmode = Mode.floatMode;

    int topPadding = 3;
    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        //DontDestroyOnLoad(gameObject);
    }

    void Start () {
        myLevelSet = levelTileSet[StaticLevelLoader.TileSetIndex].GetComponent<LevelSet>();
        mapHolder = GetComponent<MapHolder>();
        int worldIndex = StaticLevelLoader.TileSetIndex;
        startingLevel = StaticLevelLoader.MapIndex;
        Debug.Log("World " + worldIndex + " - Level " + startingLevel);
        levelInASCII = mapHolder.maps[worldIndex][startingLevel];
        
        GenerateMap();
        if(curmode == Mode.boolMode)
        {
            tilesMapBool = new bool[columns, rows];
            ConvertMapToMatrixBool();
        }
        else if(curmode == Mode.floatMode)
        {
            tilesMapFloat = new float[columns, rows];
            ConvertMapToMatrixFloat();
        }     
    }
	private void Update()
    {
        if (Input.GetButtonDown("PrintMap") && curmode == Mode.boolMode)
            DebugMapBool();
        else if(Input.GetButtonDown("PrintMap") && curmode == Mode.floatMode)
            DebugMapFloat();
    }
    private void GenerateMap()
    {
        // Make General Assertions about what needs to be in the map
        // Must have both a Left and Right ant
        if (!levelInASCII.Contains("r") || !levelInASCII.Contains("l"))
        {
            Debug.LogError("Error: there must exist both a right and a left ant in the map at any given time");
            return;
        }
        // Must have at least one egg
        if (!levelInASCII.Contains("e"))
        {
            Debug.LogError("Error: there must exist at least one egg in the map at ant given time");
            return;
        }
    
        // Count how many columns in a row
        columns = levelInASCII.IndexOf(' '); 
        rows = levelInASCII.Split(' ').Length;

        // If last character is a space then delete it. This causes extra rows to generate if not fixed
        if (levelInASCII[levelInASCII.Length - 1] == ' ')
            rows--;

        // Get Rid of all spaces in level text
        levelInASCII = levelInASCII.Replace(" ", "");

        // Pad beginning of maptext with .... for the sky
        string paddingText = "";
        for (int i = 0; i < topPadding; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                paddingText += ".";
            }
        }

        levelInASCII = paddingText + levelInASCII;

        rows += topPadding;

        // Begin the actual map generation
        boardHolder = new GameObject("Board").transform;

        int count = columns * topPadding;// start the count of the REAL map data after the padding rows have been added at the top
        for (int y = 0; y < rows + 1; y++)
        {
            for (int x = -1; x < columns+1; x++)
            {
                GameObject toInstantiate = myLevelSet.floorTiles[Random.Range(0, myLevelSet.floorTiles.Length-1)];

                // Create the wall borders, sky, and grass tiles;
                if (y == 0 || y == 1) // Sky
                    toInstantiate = myLevelSet.skyTiles[Random.Range(0, myLevelSet.skyTiles.Length-1)];
                else if(y == 2) // Grass
                    toInstantiate = myLevelSet.grassTiles[Random.Range(0, myLevelSet.grassTiles.Length-1)];
                else if (x == -1 || x == columns || y == rows) // Bordering walls
                    toInstantiate = myLevelSet.outerWallTiles[Random.Range(0, myLevelSet.outerWallTiles.Length)];              
                else if(count < levelInASCII.Length)// Now look at the level Set
                {
                    if (levelInASCII[count] == wallChar)
                        toInstantiate = myLevelSet.wallTiles[Random.Range(0, myLevelSet.wallTiles.Length-1)];
                    else if (y == 3 && levelInASCII[count] == floorChar || levelInASCII[count] == exitChar) // For first layer, any openings must be an exit tile
                        toInstantiate = myLevelSet.exitTiles[Random.Range(0, myLevelSet.exitTiles.Length-1)];

                    if (levelInASCII[count] == leftAntChar)
                        Instantiate(blueAnt, new Vector3(x, -y, 0f), Quaternion.identity);
                    else if (levelInASCII[count] == rightAntChar)
                        Instantiate(redAnt, new Vector3(x, -y, 0f), Quaternion.identity);
                    else if (levelInASCII[count] == eggChar)
                        Instantiate(myLevelSet.eggTiles[Random.Range(0, myLevelSet.eggTiles.Length)], new Vector3(x, -y, 0f), Quaternion.identity);

                    count++;

                }
                // Put tile object into the board parent
                GameObject instance = Instantiate(toInstantiate, new Vector3(x, -y, 0f), mapHolder.transform.rotation);
                instance.transform.SetParent(boardHolder);
               
            }
            doneGenerating = true;
        }

    }

    public void GetMapData(out int col, out int row, out string mapAsString)
    {
        col = columns;
        row = rows;
        mapAsString = levelInASCII;
    }

    void ConvertMapToMatrixBool()
    {
        for (int c = 0; c < columns; c++)
        {
        
            for (int r = 0; r < rows; r++)
            {
                int fakeCtr = c + (r * (columns));
                bool toPush = false; 

                if (levelInASCII[fakeCtr] == 'e' ||  levelInASCII[fakeCtr] == 'b')
                    toPush = true;
                else if(levelInASCII[fakeCtr] == 'l' )
                {
                    leftAntPos = new Vector2(c, -r);
                    toPush = false;
                }
                else if (levelInASCII[fakeCtr] == 'r')
                {
                    rightAntPos = new Vector2(c, -r);
                    toPush = false;
                }
                else if (levelInASCII[fakeCtr] == '#')
                    toPush = false;
                else if (levelInASCII[fakeCtr] == '.')
                    toPush = true;

                tilesMapBool[c, r] = toPush;
            }
           
        }
       grid = new PathFind.Grid(columns, rows, tilesMapBool);
    }
    void ConvertMapToMatrixFloat()
    {
        for (int c = 0; c < columns; c++)
        {

            for (int r = 0; r < rows; r++)
            {
                int fakeCtr = c + (r * (columns));
                float toPush = 9;
                // Less is better, but zero is unwalkable
                if (levelInASCII[fakeCtr] == 'e' || levelInASCII[fakeCtr] == 'b') //Open space
                    toPush = 1f;
                else if (levelInASCII[fakeCtr] == 'l') // Left ant
                {
                    leftAntPos = new Vector2(c, -r);
                    toPush = 1;
                }
                else if (levelInASCII[fakeCtr] == 'r') // Right ant
                {
                    rightAntPos = new Vector2(c, -r);
                    toPush = 1;
                }
                else if (levelInASCII[fakeCtr] == '#') // Wall unwalkable
                    toPush = 0;
				else if (levelInASCII[fakeCtr] == '.' || levelInASCII[fakeCtr] == 'X' && r >= topPadding)
                    toPush = 1;
				else if (levelInASCII[fakeCtr] == '.' && r < topPadding)
					toPush = 2;

                tilesMapFloat[c, r] = toPush;
            }

        }
        NewUpdateFunc(leftAntPos, 1);
        NewUpdateFunc(rightAntPos, 1);
         grid = new PathFind.Grid(columns, rows, tilesMapFloat);
    }

    private void DebugMapBool()
    {
        Debug.Log(columns + ", " + rows);
        for (int x = 0; x < columns; x++)
        {

            string row = "";
            for (int y = 0; y < rows; y++)
            {
                if(tilesMapBool[x,y])
                {
                    row += " _ ";
                }
                else
                {
                    row += " X ";
                }
                
            }
            Debug.Log(row + "; ");
        }
    }
    private void DebugMapFloat()
    {
        Debug.Log(columns + " x " + rows);
        for (int y = 0; y < rows; y++ )
        {

            string row = "";
            for (int x = 0; x < columns; x++)
            {
                string item = String.Format("{0, 6}", ("[" + tilesMapFloat[x, y]) + "]");
                row += item;
            }
            Debug.Log(row + "; ");
        }
        
    }
    public void UpdatePlayerPos(Vector2 pos, string ant)
    {
        if(curmode == Mode.boolMode)
        {
            //Debug.Log("Updating the Map with new Player spots");
            if (ant == "Right")
            {
                if (pos != rightAntPos)
                {
                    tilesMapBool[(int)rightAntPos.x, -(int)rightAntPos.y] = true;
                    rightAntPos = pos;
                    tilesMapBool[(int)rightAntPos.x, -(int)rightAntPos.y] = false;
                }
            }
            else if (ant == "Left")
            {
                if (pos != leftAntPos)
                {
                    tilesMapBool[(int)leftAntPos.x, -(int)leftAntPos.y] = true;
                    leftAntPos = pos;
                    tilesMapBool[(int)leftAntPos.x, -(int)leftAntPos.y] = false;
                }
            }
            this.grid = new PathFind.Grid(columns, rows, tilesMapBool);
        }
        else if(curmode == Mode.floatMode)
        {
            //Debug.Log("Updating the Map with new Player spots");
            if (ant == "Right")
            {
                if (pos != rightAntPos)
                {
                    NewUpdateFunc(rightAntPos, -1);
                    rightAntPos = pos;
                    NewUpdateFunc(rightAntPos, 1);
                }
            }
            else if (ant == "Left")
            {
                if (pos != leftAntPos)
                {
                    NewUpdateFunc(leftAntPos, -1);
                    leftAntPos = pos;
                    NewUpdateFunc(leftAntPos, 1);
                }
            }
            this.grid = new PathFind.Grid(columns, rows, tilesMapFloat);
        }
      
    }
 
    void NewUpdateFunc(Vector2 center, int addorsub)
    {
        int[] scoreAry = { 80, 40, 20, 10, 5, 2};
        int rad = scoreAry.Length;
        for(int i = -rad; i <= rad; i++)// X Axis
        {
            int upDownDistance = 0;
            if (i != 0) // if on the negative side, add to upDownDist
            {
                upDownDistance = Mathf.Abs((int)(i + (-rad * Mathf.Sign(i))));
                int val = scoreAry[(Mathf.Abs(i) - 1)];
                if (CanOverwriteSpace(new Vector2((int)center.x + i, -(int)center.y)))
                {
                    tilesMapFloat[(int)center.x + i, -(int)center.y] += val * addorsub;
                }
                
            }
            else if(i == 0)
            {
                upDownDistance = rad;
                if (CanOverwriteSpace(new Vector2((int)center.x + i, -(int)center.y)))
                {
                    tilesMapFloat[(int)center.x + i, -(int)center.y] += 10000 * addorsub; // Position of player so it is super large
                }
            }

            if (upDownDistance > 0)
            {
                for (int j = -upDownDistance; j <= upDownDistance; j++) // Y Axis
                {
                    if (j != 0) // if on the negative side, add to upDownDist
                    {
                        if (CanOverwriteSpace(new Vector2((int)center.x + i, -(int)center.y + j)))
                        {
                            tilesMapFloat[(int)center.x + i, -(int)center.y + j] += scoreAry[Mathf.Abs(i) - 1 + Mathf.Abs(j)] * addorsub;
                        }
                    }
                }
            }
        }
    }

    bool CanOverwriteSpace(Vector2 newPos)
    {
        bool notZero = true;
        // Assert that not going off map
        // X Axis test
        bool xWorks = newPos.x >= 0 && newPos.x < columns;
        bool yWorks = newPos.y >= 0 && newPos.y < rows;
        // Can't overwrite if its a wall, aka 0 space
        if (yWorks && xWorks)
            notZero = tilesMapFloat[(int)newPos.x, (int)newPos.y] != 0f;

        return xWorks && yWorks && notZero;
    }
}
