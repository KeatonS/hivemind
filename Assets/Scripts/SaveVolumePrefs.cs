﻿using UnityEngine.UI;
using UnityEngine;

public class SaveVolumePrefs : MonoBehaviour {

    public Slider masterVolSlider;
    public Slider musicVolSlider;
    public Slider sfxVolSlider;
	
    public void SubmitSliderSettings()
    {
        PlayerPrefs.SetFloat("MasterVol", masterVolSlider.value);
        PlayerPrefs.SetFloat("MusicVol", musicVolSlider.value);
        PlayerPrefs.SetFloat("SFXVol", sfxVolSlider.value);     
        AudioManager.instance.UpdateSoundSettings();
    }

    public void GetSliderSettings()
    {
        masterVolSlider.value = PlayerPrefs.GetFloat("MasterVol");
        musicVolSlider.value = PlayerPrefs.GetFloat("MusicVol");
        sfxVolSlider.value = PlayerPrefs.GetFloat("SFXVol");
    }

    void OnEnable()
    {
        GetSliderSettings();
        AudioManager.instance.UpdateSoundSettings();
    }

    public void MakeSoundEffect()
    {
        AudioManager.instance.PlaySoundEffectChange("SoundEffectSliderChange");
    }
}
