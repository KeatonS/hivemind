﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticLevelLoader {

    public static int TileSetIndex { get; set; }

    public static int WorldIndex { get; set; }
    public static int MapIndex { get; set; }
}
