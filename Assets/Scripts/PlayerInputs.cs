﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputs : MovingObject
{ 
    public string side = "Left";
    public LeftJoystick leftJoystick; // the game object containing the LeftJoystick script
    public RightJoystick rightJoystick; // the game object containing the RightJoystick script
    private Vector3 leftJoystickInput; // holds the input of the Left Joystick
    private Vector3 rightJoystickInput; // hold the input of the Right Joystick
    private int horizontal;
    private int vertical;
    private Vector2 lastInput;
    private float joystickDeadZone = 0.1f;

    void Awake()
    {
        leftJoystick = GameObject.Find("Left Joystick").GetComponent<LeftJoystick>();
        rightJoystick = GameObject.Find("Right Joystick").GetComponent<RightJoystick>();
    }

    void FixedUpdate()
    {
        if((Vector2)transform.position == pos)
        {
            lastInput.y = vertical;
            lastInput.x = horizontal;
        }   
        else if((Vector2)transform.position != pos)
        {
            //Move(this.transform.position, pos - (Vector2)this.transform.position);
            SmoothMovement(pos - (Vector2)this.transform.position);
        }
        if(lastInput != Vector2.zero)
        {
            Move(pos, lastInput);        
        }
            
       
    }

    void Update()
    {
#if UNITY_STANDALONE || UNITY_WEBPLAYER
        horizontal = (int)Input.GetAxisRaw("Horizontal(" + side + ")");
        vertical = (int)Input.GetAxisRaw("Vertical(" + side + ")");
        
        if ((Vector2)transform.position == (pos + lastDirection) && lastInput != null)
        {
            
            pos += lastDirection;
            MapGenerator.instance.UpdatePlayerPos(pos, side);
        }

#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
        // get input from both joysticks
        leftJoystickInput = leftJoystick.GetInputDirection();
        rightJoystickInput = rightJoystick.GetInputDirection();
        
        Vector3 curJoystickInput = leftJoystickInput;
        if (side == "Right")
        {
            curJoystickInput = rightJoystickInput;
        }

        Vector3 normal = Vector3.right;
        float curAngle = 0f;
        if(curJoystickInput.x > 0)
        {
            normal = Vector3.right;
            curAngle = Vector3.Angle(normal, curJoystickInput);
            
            Vector3 newDirec = GetPairOfDirecs(curAngle);
            horizontal = (int)(newDirec.x * Mathf.Sign(curJoystickInput.x));
            vertical = (int)(newDirec.y * Mathf.Sign(curJoystickInput.y));
        }
        else if (curJoystickInput.x < 0)
        {
            normal = Vector3.left;
            curAngle = Vector3.Angle(normal, curJoystickInput);
            
            Vector3 newDirec = GetPairOfDirecs(curAngle);
            horizontal = (int)(newDirec.x * Mathf.Sign(curJoystickInput.x));
            vertical = (int)(newDirec.y * Mathf.Sign(curJoystickInput.y));
        }
        else if(curJoystickInput.x == 0 && curJoystickInput.y == 0)
        {
            horizontal = 0;
            vertical = 0;
        }

        if ((Vector2)transform.position == (pos + lastDirection) && lastInput != null)
        {

            pos += lastDirection;
            MapGenerator.instance.UpdatePlayerPos(pos, side);
        }
#endif //End of mobile platform dependendent compilation section started above with #elif
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
        }
        else if (col.gameObject.tag == "Enemy")
        {
            col.gameObject.GetComponent<EnemyAI>().Die();
        }
        else if (col.gameObject.tag == "Egg")
        {
            SmoothMovement(pos);
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Egg")
        {
            Move(this.transform.position, pos - (Vector2)this.transform.position);
        }
    }

    Vector3 GetPairOfDirecs(float currentAngle)
    {
        Vector3 returnVec = Vector3.right;
        float firstAngle = 45f;
        float secondAngle = 45f;

        if (currentAngle < firstAngle)
            returnVec = new Vector3(1, 0, 0);
        else if (currentAngle >= firstAngle && currentAngle <= secondAngle)
            returnVec = new Vector3(1, 1, 0);
        else if (currentAngle > secondAngle)
            returnVec = new Vector3(0, 1, 0);

        return returnVec;
    }


    public void Die()
    {
        AudioManager.instance.Play("PickUpEgg");
        pos = GameManager.instance.OnPlayerDeath(this.gameObject);
    }

}
