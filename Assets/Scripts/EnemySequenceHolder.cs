﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySequenceHolder : MonoBehaviour {
    [HideInInspector]
    public
        List<List<string>> enemySequences = new List<List<string>>
        {
            // World 1 Sequences
            new List<string>{
                // Map 0 
                "WP",

                // 1
                "WP",

                // 2
                "WP"
            },

            // World 2 Sequences
            new List<string>{
                // Map 0
                "WP - WP ; WP - WP , WP ; WP WP - WP WP , WP ",

                // Map 1
                "WP - WP ; WP - WP , WP ; WP WP - WP WP , WP ",

                // Map 2
                "WP - WP ; WP - WP , WP ; WP WP - WP WP , WP "
            },

            // World 3 Sequences
            new List<string>{
                // Map 0
                "WP - WP ; WP - WP , WP ; WP WP - WP WP , WP ",

                // Map 1
                "WP - WP ; WP - WP , WP ; WP WP - WP WP , WP ",

                // Map 2
                "WP WP - WP WP ; WP WP WP , WP - WP WP WP WP , WP WP WP WP WP - WP WP WP WP ; WP WP WP WP WP WP WP WP WP - WP WP , WP WP WP "
            }
        };
}