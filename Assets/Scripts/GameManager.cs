﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    [HideInInspector] public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
                                                                              
    // All Enemy Prefabs go here
    public GameObject waspPurple;
    public int curLevelSequenceIndex = 0;// Used for the Sequence index in the sequence holder
    delegate void EventDelegate();
    List<EventDelegate> eventList;

    List<string> gameSequence;
    static string waspPurpleCode = "WP";
    static string waspGreenCode = "WG";
    List<string> enemySpawnCode = new List<string>(new string[] { waspPurpleCode, waspGreenCode });
    List<GameObject> eggList;
    List<GameObject> enemyToEggList; // list holding the enemies targeting which egg. Kept in parallel to eggList
    
    List<GameObject> enemySpawnList;
    public List<GameObject> aliveEnemies;

    List<int> indexesOfWaveEnds;
  
    int curSeqIndex = 0;
    int curEnemyIndexInWave = 0;
    int curRound = 1;
    string levelAndMapKey;
    float timer = 0f;

    // UI Elements
    public Text roundText;
    public Text scoreText;
    public Text levelAndRoundText;

    void Awake()
    { 
        //Check if instance already exists
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        //DontDestroyOnLoad(gameObject);

    }
    // Use this for initialization
    void Start () {
        string rawSeq = GetComponent<EnemySequenceHolder>().enemySequences[StaticLevelLoader.TileSetIndex][StaticLevelLoader.MapIndex];
        levelAndMapKey = (StaticLevelLoader.TileSetIndex) + "/" + (StaticLevelLoader.MapIndex);
        gameSequence = ConvertSequenceToList(rawSeq);
        
        enemySpawnList = new List<GameObject>();
        eventList = new List<EventDelegate>();
        indexesOfWaveEnds = new List<int>();
     
        aliveEnemies = new List<GameObject>();
        eggList = new List<GameObject>();
        eggList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Egg"));
        enemyToEggList = new List<GameObject>();
        levelAndRoundText.text = "WAVES: " + curRound + "/" + 3;
        scoreText.text = "EGGS: " + eggList.Count;
        FillDelegateList();
        roundText.enabled = false;
    }
	
	// Update is called once per frame
	private void Update () {
       
        if (MapGenerator.instance.doneGenerating )
        {
            eggList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Egg"));
            foreach (GameObject egg in eggList)
                enemyToEggList.Add(null);
            scoreText.text = "EGGS: " + eggList.Count;
            if (curSeqIndex < eventList.Count)
            {
                eventList[curSeqIndex](); // Execute the current event
            }
        }
	}

    public void OnEnemyDeath(GameObject enemy, GameObject eggHeld)
    {
        FindObjectOfType<AudioManager>().Play("PurpleWaspDeath");
        if (enemy.transform.position.y == this.gameObject.transform.position.y && eggHeld != null)// enemy was at the top of screen with an egg at death
        {
            aliveEnemies.Remove(enemy);
            enemyToEggList.Remove(enemy);
            Destroy(enemy);

            eggList.Remove(eggHeld);
            Destroy(eggHeld);

            if(eggList.Count == 0)
            {
                eventList.Clear();
                curSeqIndex = 0;
                eventList.Add(GameOverFail);
            }
        }
        else if (eggHeld == null)
        {
            aliveEnemies.Remove(enemy);
            enemyToEggList.Remove(enemy);
            Destroy(enemy);
        }
    }

    public Vector2 OnPlayerDeath(GameObject player)
    {
        FindObjectOfType<AudioManager>().Play("PurpleWaspDeath");
        GameObject nextEgg = eggList[0];

        if (eggList.Count > 0)
        {
            player.transform.position = nextEgg.transform.position;
            eggList.Remove(nextEgg);
            Destroy(nextEgg);
        }
        if (eggList.Count == 0)
        {
            eventList.Clear();
            curSeqIndex = 0;
            eventList.Add(GameOverFail);
        }
        

        return nextEgg.transform.position;
    }

    bool AllEnemiesDead()
    {
        return aliveEnemies.Count == 0;
    }

    //------------- Event Methods ----------//

    void LevelOver() // DELEGATE FUNCTION
    {
        if (AllEnemiesDead())
            curSeqIndex++;
    }

    void GameOverSuccess() // DELEGATE FUNCTION
    {
        if (AllEnemiesDead())
        {
            AudioManager.instance.PlaySong("WinTheme");
            roundText.text = "YOU WIN";
            roundText.enabled = true;
            Debug.Log(levelAndMapKey + "SCORE");
            PlayerPrefs.SetInt(levelAndMapKey + "SCORE", eggList.Count);
        }
    }

    void GameOverFail() // DELEGATE FUNCTION
    {
        AudioManager.instance.PlaySong("GameOverTheme");    
        roundText.text = "GAME OVER";
        roundText.enabled = true;
    }
   
    void SpawnEnemy() // DELEGATE FUNCTION FOR "WP OR OTHER ENEMY CODE"
    {
        // Get Earliest enemy in the spawn list and then remove it
        GameObject enemyBeingSpawned = enemySpawnList[0];
        enemySpawnList.RemoveAt(0);

        // Increment the amount of enemies in this wave. (This value resets to zero when a - or ; is executed, indicating a change in waves
        curEnemyIndexInWave++;
       
        int eggIndices = eggList.Count;

        int targetIndex = curEnemyIndexInWave % eggIndices;
        //Debug.Log(" Enemy: " + curEnemyIndexInWave + " is targeting Egg #" + targetIndex);
        float[] sideToSpawn = { 0, -1, 1, 2, -2, 3, -3, 4, -4 };

        // create source and target points
        PathFind.Point _from = new PathFind.Point((int)(this.transform.position + new Vector3(sideToSpawn[curEnemyIndexInWave], 0f)).x, -1 * (int)this.transform.position.y);
        PathFind.Point _to = new PathFind.Point((int)eggList[targetIndex].transform.position.x, -1 * (int)eggList[targetIndex].transform.position.y);
        // get path
        // path will either be a list of Points (x, y), or an empty list if no path is found.
        List<PathFind.Point> path = PathFind.Pathfinding.FindPath(MapGenerator.instance.grid, _from, _to);

        //*************************************
        GameObject newGuy = (Instantiate(enemyBeingSpawned, this.transform.position + new Vector3(sideToSpawn[curEnemyIndexInWave], 0f), this.transform.rotation));
        //***************************************

        newGuy.GetComponent<EnemyAI>().target = eggList[targetIndex];

        enemyToEggList[targetIndex] = newGuy;

        List<Vector2> hold = new List<Vector2>();
        foreach (PathFind.Point p in path)
        {
            hold.Add(new Vector2(p.x, p.y));
        }

        newGuy.GetComponent<EnemyAI>().pathList = hold;
        aliveEnemies.Add(newGuy);

        curSeqIndex++;
    }

    void SpawnAfterAllDead() // DELEGATE FUNCTION FOR " - "
    {
        //Debug.Log("Buffering time between enemy waves");
        if (AllEnemiesDead())
        {
            float time = 3f;
            timer += Time.deltaTime;
            if (timer >= time)
            {
                timer = 0;
                curSeqIndex++;
                curEnemyIndexInWave = 0;
                return;
            }
        }
    }

    void RoundBuffer() // DELEGATE FUNCTION FOR " ; "
    {
        //Debug.Log("Buffering time between rounds");
        if (AllEnemiesDead())
        {
            AudioManager.instance.PlaySong("RoundChangeTheme");
            float time = AudioManager.instance.GetSongLength("RoundChangeTheme");
            timer += Time.deltaTime;
            roundText.text = "ROUND " + (curRound);
            levelAndRoundText.text = "WAVES: " + curRound + "/" + 3;
            roundText.enabled = true;
            if (timer >= time)
            {
                AudioManager.instance.PlaySong("World" + (StaticLevelLoader.TileSetIndex + 1).ToString() + "Theme");
                curRound++;
                roundText.enabled = false;
                timer = 0;
                curSeqIndex++;
                curEnemyIndexInWave = 0;
                return;
            }
        }
    }

    void SpawnAfterSetTime() // DELEGATE FUNCTION FOR " , "
    {
        //Debug.Log("Waiting to Spawn more enemies this Wave");
        float time = 3f;
        timer += Time.deltaTime;
        if (timer >= time)
        {
            timer = 0;
            curSeqIndex++;
            return;
        }
    }

    //------------- End Event Methods ----------//
    

    void FillDelegateList() // CONVERT ASCII SEQUENCE INTO EXECUTION LIST
    {
        for (int i = -1; i < gameSequence.Count; i++)
        {      
            GameObject enemyToAdd = null;
            if(i == -1)
            {
                eventList.Add(RoundBuffer);
            }
            else if(i >= 0)
            {
                if (enemySpawnCode.Contains(gameSequence[i])) // is an enemy code
                {
                    eventList.Add(SpawnEnemy);
                    if (gameSequence[i] == waspPurpleCode)
                        enemyToAdd = waspPurple;
                    else
                        Debug.LogError("Error, unhandled enemy Code to spawn");

                    enemySpawnList.Add(enemyToAdd);
                }
                else if (gameSequence[i] == "-") // All enemies on screen must die before next wave within round
                {
                    eventList.Add(SpawnAfterAllDead);
                    indexesOfWaveEnds.Add(i);                  
                }
                else if (gameSequence[i] == ";") // Marks the end of a round. All enemies on screen must be dead.
                {
                    eventList.Add(RoundBuffer);
                    indexesOfWaveEnds.Add(i);           
                }
                else if (gameSequence[i] == ",") // Waits a certain amount of time before more enemies are spawned within the current wave. Not dependent on all enemies being dead
                {
                    eventList.Add(SpawnAfterSetTime);
                }
            }
            
        }

        eventList.Add(LevelOver);
        eventList.Add(GameOverSuccess);
    }

    void OnDisable()
    {
        eventList.Clear();
    }

    List<string> ConvertSequenceToList(string sequence) // Converts ACII string into a list that is then used by FillDelegateList method.
    {
        string[] ssize = sequence.Split(new char[0]);
        List<string> returnString = new List<string>();
        for (int i = 0; i < ssize.Length; i++)
        {
            returnString.Add(ssize[i]);
        }

        return returnString;
    }

    // Public facing function used in EnemyAI so all other enemies know which ones are being targeted
    public GameObject giveEnemyNewTarget(GameObject enemy) // This function finds the next closest egg to an enemy if they can not reach the egg they are currently targeting due to player positioning
    {
        EnemyAI ai = enemy.GetComponent<EnemyAI>();
        Vector2 position = enemy.GetComponent<Rigidbody2D>().transform.position;
        GameObject curTarg = ai.target;
        List<GameObject> possibleEggs = new List<GameObject>();
        for(int i = 0; i < eggList.Count; i++)
        {
            if(enemyToEggList[i] == null && curTarg != enemyToEggList[i])
            {
                possibleEggs.Add(eggList[i]);
            }
        }
        GameObject retEgg = possibleEggs[0];
        float shortestDist = Vector2.Distance(position, possibleEggs[0].transform.position);
        float x;
        foreach (GameObject egg in possibleEggs)
        {
            if((x = Vector2.Distance(position, egg.transform.position)) < shortestDist)
            {
                shortestDist = x;
                retEgg = egg;
            }
                
        }

        return retEgg;
    }
    
}
