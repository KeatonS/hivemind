﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour {

    public string currentStateString = "";
    public List<GameObject> eggList;
    public List<GameObject> curEnemyList;
    public List<string> sequenceList;
    public int sequenceToExecute = 0;

    // All Enemy Prefabs go here
    public GameObject waspPurple;

    private string sequence;
    private int curSeqIndex = 0;
    private float timer = 0f;
	// Use this for initialization
	void Start () {
        curEnemyList = new List<GameObject>();

        sequenceList = new List<string>();
        sequence = GetComponent<EnemySequenceHolder>().enemySequences[StaticLevelLoader.WorldIndex][StaticLevelLoader.MapIndex];
        sequenceList = ConvertSequenceToList(sequence);

        eggList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Egg"));
    }
	
	// Update is called once per frame
	void Update () {

        if(curSeqIndex < sequenceList.Count)
        {
            currentStateString = sequenceList[curSeqIndex];

            if (sequenceList[curSeqIndex] == "WP")
            {
                SpawnPurpleWasp();
            }
            else if (sequenceList[curSeqIndex] == "-")
            {
                if (AllEnemiesDead())
                {
                    RoundTimeBuffer(3f);
                }

            }
            else if (sequenceList[curSeqIndex] == ";")
            {
                if (AllEnemiesDead())
                {
                    RoundComplete();
                    RoundTimeBuffer(3f);
                }         
            }
            else if (sequenceList[curSeqIndex] == ",")
            {
                RoundTimeBuffer(3f);
            }
        }
        else if(curSeqIndex >= sequenceList.Count && AllEnemiesDead())
        {
            LevelOver();
        }

        if(eggList.Count <= 0)
        {
            GameOver();
        }
    }

    void SpawnPurpleWasp()
    {
        Debug.Log("Spawning a Purple Wasp");
        if(eggList.Count > 0)
        {
            curEnemyList.Add(Instantiate(waspPurple, new Vector2(this.transform.position.x + curEnemyList.Count, this.transform.position.y), this.transform.rotation));
            curSeqIndex++;
        }
        
    }

    public void OnEnemyDeath(GameObject enemy, GameObject eggHeld)
    {
        if(enemy.transform.position.y == this.gameObject.transform.position.y && eggHeld != null)// enemy was at the top of screen with an egg at death
        {
            curEnemyList.Remove(enemy);
            Destroy(enemy);

            eggList.Remove(eggHeld);
            Destroy(eggHeld);
        }
        else if(eggHeld == null)
        {
            curEnemyList.Remove(enemy);
            Destroy(enemy);
        }
    }

    bool AllEnemiesDead()
    {
        return curEnemyList.Count == 0;
    }

    void RoundTimeBuffer(float time)
    {
        timer += Time.deltaTime;

        if(timer >= time)
        {
            timer = 0;
            curSeqIndex++;
            return;
        }
    }

    void RoundComplete()
    {
        Debug.Log("Round Complete");
    }

    void LevelOver()
    {
        Debug.Log("Level Over");
    }

    void GameOver()
    {
        Debug.Log("Game Over");
    }

    List<string> ConvertSequenceToList(string sequence)
    {
        string[] ssize = sequence.Split(new char[0]);
        List<string> returnString = new List<string>();
        for(int i = 0; i < ssize.Length; i++)
        {
            returnString.Add(ssize[i]);
        }

        return returnString;
    }
}
