﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleScriptCode : MonoBehaviour {

    /*title Simple Block Pushing Game
author Stephen Lavelle
homepage www.puzzlescript.net

========
OBJECTS
========

Background
LIGHTGREEN GREEN
11111
01111
11101
11111
10111


Target
DarkBlue
.....
.000.
.0.0.
.000.
.....

Wall
BROWN DARKBROWN
00010
11111
01000
11111
00010

LeftAnt
Black Red White Red
.000.
.111.
22222
.333.
.3.3.

RightAnt
Black Blue White Blue
.000.
.111.
22222
.333.
.3.3.

Crate
Orange Yellow
00000
0...0
0...0
0...0
00000

Eyeball
WHITE BLUE BLACK
00000
01110
01210
01110
00000

=======
LEGEND
=======

. = Background
# = Wall
L = LeftAnt
R = RightAnt
* = Crate
@ = Crate and Target
O = Target


=======
SOUNDS
=======

Crate MOVE 36772507

================
COLLISIONLAYERS
================

Background
Target
LeftAnt, RightAnt, Wall, Crate, Eyeball

======
RULES
======

[ >  LeftAnt | Crate ] -> [  >  LeftAnt | > Crate  ]
[ Eyeball | ... | LeftAnt ] -> [ > Eyeball | ... | LeftAnt ]


==============
WINCONDITIONS
==============

All Target on Crate

=======
LEVELS
=======


######
#....#
#....#
#....#
#L..R#
#....#
######





    */

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
