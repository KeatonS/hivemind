﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseAndResumeOnClick : MonoBehaviour {

    private bool isPaused = false;

    void Start()
    {
        isPaused = false;
    }
    public void PauseOrResume()
    {
        if(isPaused == false) // Pause
        {
            Time.timeScale = 0.0f;
            isPaused = true;
        }
        else if(isPaused == true) // Resume
        {
            Time.timeScale = 1.0f;
            isPaused = false;
        }
    }
    
}
