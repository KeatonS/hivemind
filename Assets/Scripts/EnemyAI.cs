﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MovingObject {

    private GameObject enemyManager;
    private GameObject eggCarried = null;

    public List<Vector2> pathList;
    public GameObject target = null;
    float eggHeldSpeedMod = 0.65f;
    int curPathSpace = -1;
    Vector2 posHold;
    Vector2 nextPos;
    Vector2 escapeSquare = new Vector2(5,0);
    bool eggFound = false;
    bool escapedSafely = false;
    bool invincibleStart = true;
    bool invincible = true;
    float invincibleTime = 7.0f;
    float invincibleTimer = 0.0f;
    bool collidedWithEgg = false;
    
   
    GameObject [] players;
    SpriteRenderer rend;
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        curPathSpace = 0;
        players = GameObject.FindGameObjectsWithTag("Player");
        InvokeRepeating("CheckForPlayer", 1f, 0.05f);
        rend = this.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update () {

        Invincibility();

        if (pathList.Count == 0 )
        {
            List<Vector2> hold = pathList;
            curPathSpace = 0;

            for (int i = 0; i < hold.Count; i++) // Make the y values negative
            {
                pathList.Add( new Vector2(hold[i].x, -1 * hold[i].y));
            }
        }
        else if(pathList.Count > 0)
        {
            // if current position equal the current index of the path list, increment the path list, try moving to the next position
            if (curPathSpace < pathList.Count && !eggFound)
            {
                FollowPath(1);
            }
            else if (eggFound && !escapedSafely)
            {
                FollowPath(1);
            }
            else if (eggFound && escapedSafely)
            {
                WaspEscape();
            }

            if ((posHold == escapeSquare) && eggFound && !escapedSafely) // if at enemy manager, then wasp escaoed safely
            {
                escapedSafely = true;
            }

            if (eggCarried != null)
            {
                eggCarried.transform.position = this.transform.position;
            }
            if (eggCarried & invincible) invincible = false;
        }
       
    }

    void FollowPath(int direct)
    {
        if(curPathSpace < pathList.Count && curPathSpace >=0)
        {
            if ((Vector2)this.transform.position == pathList[curPathSpace])
            {
                curPathSpace += direct;
                posHold = this.transform.position;
                if (curPathSpace < pathList.Count && curPathSpace >= 0) nextPos = pathList[curPathSpace];
                else curPathSpace = 0;
            }
            else if ((Vector2)this.transform.position != pathList[curPathSpace])
            {
                Move(posHold, nextPos - posHold);
            }
            //else if((Vector2)this.transform.position == nextPos) nextPos = pathList[curPathSpace];
        }
      
    }

    void FixedUpdate()
    {
        
        if (pathList.Count != 0 && collidedWithEgg == true && eggFound == false && Vector2.Distance((Vector2)this.transform.position,pathList[pathList.Count - 1]) <= Mathf.Epsilon)
        {

            Debug.Log("Recalculating path to exit now!!");
            eggCarried = target;
            RecalculatePath(escapeSquare);
            eggFound = true;
        }
    }

    void WaspEscape()
    {
        KillSelf();
    }

    void OnTriggerEnter2D(Collider2D col)
    {      
        if (col.gameObject == target && collidedWithEgg == false)
        {
            AudioManager.instance.Play("PickUpEgg");
            collidedWithEgg = true;
            this.speed = this.speed * eggHeldSpeedMod;

            Debug.Log("Collided with the Egg!!!");
        }

        if (col.gameObject.tag == "Player" && invincible)
        {
            col.gameObject.GetComponent<PlayerInputs>().Die();
        }
    }

    public void Die()
    {   if(invincible == false)
        {
            if (eggCarried != null) eggCarried.transform.position = new Vector2(Mathf.RoundToInt(eggCarried.transform.position.x), Mathf.RoundToInt(eggCarried.transform.position.y));
            GameManager.instance.OnEnemyDeath(this.gameObject, null);
           // FindObjectOfType<AudioManager>().Play("PurpleWaspDeath");
        }
    }

    public void KillSelf()
    {
        GameManager.instance.OnEnemyDeath(this.gameObject, eggCarried);
    }

    void CheckForPlayer()
    {
        
        foreach (GameObject player in players)
        {
            Vector2 playerPos = new Vector2(Mathf.Floor(player.transform.position.x), Mathf.Floor(player.transform.position.y));

            if (pathList.Contains(playerPos) && (Vector2)this.transform.position == pathList[curPathSpace])
            {
                if(!eggCarried)
                    RecalculatePath(target.transform.position);
                else
                    RecalculatePath(escapeSquare);
            }
        }
        
    }

    void RecalculatePath(Vector2 newGoal)
    {
       
        PathFind.Point _from = new PathFind.Point(this.transform.position);
        PathFind.Point _to = new PathFind.Point(newGoal);
        
        List<PathFind.Point> path = PathFind.Pathfinding.FindPath(MapGenerator.instance.grid, _from, _to);
        if(path.Count == 0)
        {
            GameObject newTarget = null;
            newTarget = GameManager.instance.giveEnemyNewTarget(this.gameObject);
            PathFind.Point _newto = new PathFind.Point(newTarget.transform.position);
            path = PathFind.Pathfinding.FindPath(MapGenerator.instance.grid, _from, _newto);
        }
        List<Vector2> hold = new List<Vector2>();
        foreach (PathFind.Point p in path)
        {
            hold.Add(new Vector2(p.x, p.y));
        }
        posHold = new Vector2(hold[0].x, -hold[0].y);
        hold.RemoveAt(0);
        this.pathList = hold;
        curPathSpace = 0;
        
        if(pathList.Count != 0)
        {
            nextPos = new Vector2(pathList[0].x, pathList[0].y);
        }
        else
        {
            Debug.LogError("Enemy had no other option but to self destruct!!!");
            KillSelf();
        }
       
    }

    void Invincibility()
    {
        if (invincibleStart)
        {
            invincibleTimer = Time.time;
            invincibleStart = false;
            StartCoroutine(Flash());
        }
        else if (!invincibleStart)
        {
            if (Time.time - invincibleTimer <= invincibleTime && invincible)
            {
                
            }
            else
            {
                rend.color = Color.white;
                invincible = false;
            }
        }
    }
    IEnumerator Flash()
    {
        while(invincible)
        {
            rend.color = Color.red;
            yield return new WaitForSeconds(0.15f);
            rend.color = Color.white;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
