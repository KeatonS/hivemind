﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour
{
    public float speed = 2.0f;                         // Speed of movement
    public LayerMask wall;
  
    private BoxCollider2D boxCollider;
    private SpriteRenderer spriteRend;
    protected Vector2 lastDirection;
    protected Vector2 pos;                                // For movement

    protected virtual void Start()
    {
        pos = transform.position;          // Take the initial position
        boxCollider = GetComponent<BoxCollider2D>();
        spriteRend = GetComponent<SpriteRenderer>();
    }

    protected virtual bool Move(Vector2 start, Vector2 direction)
    {
        Vector2 verticalSpot = new Vector2(0, direction.y);
        Vector2 horizontalSpot = new Vector2(direction.x, 0);
        Vector2 verticalCast = new Vector2(0, direction.y * 1.48f);
        Vector2 horizontalCast = new Vector2(direction.x * 1.48f, 0);

        boxCollider.enabled = false;
        RaycastHit2D hitY = Physics2D.Linecast(start + direction * 0.5f, start + verticalCast, wall);
        RaycastHit2D hitX = Physics2D.Linecast(start + direction * 0.5f, start + horizontalCast, wall);

        boxCollider.enabled = true;

        Vector2 destination = Vector2.zero ;
        if (hitY.transform == null && Mathf.Abs(direction.y) >= 1) destination = verticalSpot;
        else if (hitX.transform == null && Mathf.Abs(direction.x) >= 1) destination = horizontalSpot;

        if (destination != Vector2.zero)
        {
            lastDirection = destination;
            RotateSprite();
            StartCoroutine(SmoothMovement(start + destination));
            return true;
        }

        return false;
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer == 10) // entitie
        {
            Debug.LogError(this.gameObject + "Entered into a wall somehow");
        }
        
    }
    protected virtual IEnumerator SmoothMovement(Vector3 end)
    {

        transform.position = Vector2.MoveTowards(transform.position, end, Time.deltaTime * speed);    // Move there 
        yield return null;

    }

    protected virtual void RotateSprite()
    {
        float rotateAngle = 0f;
        if (lastDirection.x < 0) rotateAngle = 90;
        else if (lastDirection.y < 0) rotateAngle = 180;
        else if(lastDirection.x > 0) rotateAngle = 270;
        else if(lastDirection.y > 0) rotateAngle = 0;

        spriteRend.transform.localRotation = Quaternion.Euler(0, 0, rotateAngle);
    }

}
