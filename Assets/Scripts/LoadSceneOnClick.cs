﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

    public void LoadByIndex(int sceneIndex)
    {
       
        if (sceneIndex == 0)
        {
            GameManager.instance.gameObject.SetActive(false); // This line fixes the bug of the game manager reloading a song on the next scene. IDK why
            AudioManager.instance.PlaySong("MainMenuTheme");      
        }
           
        SceneManager.LoadScene(sceneIndex);
      
    }

    public void LoadTileSet(int tileSetIndex)
    {
        StaticLevelLoader.TileSetIndex = tileSetIndex;
    }

    public void LoadMap(int mapIndex)
    {
        StaticLevelLoader.MapIndex = mapIndex;
    }
}
