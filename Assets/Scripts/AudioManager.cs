﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public Sound[] sounds;
    public Sound[] songs;

    private AudioSource currentSongSource = null;

    public static AudioManager instance;
    float masterMod = 1;
    float musicMod = 1;
    float sfxMod = 1;

    // Use this for initialization
    void Awake () {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        masterMod = PlayerPrefs.GetFloat("MasterVol");
        musicMod = PlayerPrefs.GetFloat("MusicVol") * masterMod;
        sfxMod = PlayerPrefs.GetFloat("SFXVol") * masterMod;

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume * sfxMod;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }

        foreach (Sound s in songs)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume * musicMod;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }
	
    void Start()
    {
        PlaySong("MainMenuTheme");     
    }

 
	public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogError("No audio clip found with name " + name);
            return;
        }
        s.source.volume = s.volume * sfxMod;
        s.source.pitch = s.pitch;

        s.source.Play();
        
    }

    // maintains one channel for songs
    public void PlaySong(string name)
    {
       
        Sound s = Array.Find(songs, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogError("No song found with name " + name);
            return;
        }
        else
        {
            if (currentSongSource == null)
            {
                currentSongSource = s.source;
            }   
            else if(currentSongSource == s.source)
            {
                return;
            }
            else
            {
                currentSongSource.Stop();
                currentSongSource = s.source;
            }           
        }
        s.source.volume = s.volume * musicMod;
        s.source.pitch = s.pitch;

        s.source.Play();
        
    }

    public float GetSongLength(string name)
    {
        Sound s = Array.Find(songs, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogError("No song found with name " + name);
            return 0f;
        }

        return s.clip.length;
    }

    public float GetSoundLength(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogError("No song found with name " + name);
            return 0f;
        }

        return s.clip.length;
    }

    public void UpdateSoundSettings()
    {
        masterMod = PlayerPrefs.GetFloat("MasterVol");
        musicMod = PlayerPrefs.GetFloat("MusicVol") * masterMod;
        sfxMod = PlayerPrefs.GetFloat("SFXVol") * masterMod;
        
        foreach (Sound s in sounds)
        {          
            s.source.volume = s.volume * sfxMod;
        }

        foreach (Sound s in songs)
        {
            s.source.volume = s.volume * musicMod;
        }
    }

    public void PlaySoundEffectChange(string name)
    {
        
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogError("No song found with name " + name);
            return;
        }
        s.source.Stop();
        s.source.volume = s.volume * sfxMod;
        s.source.pitch = s.pitch;

        s.source.Play();
        
        
    }

}
